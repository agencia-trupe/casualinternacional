<?php

return [
    'pagina-nao-encontrada' => 'PAGE NOT FOUND',

    'nav' => [
        'produtos'   => 'Products',
        'marcas'     => 'Brands',
        'novidades'  => 'What\'s new',
        'perfil'     => 'Profile',
        'designers'  => 'Designers',
        'contato'    => 'Contact',
        'catalogo'   => 'Professional Area',
        'interiores' => 'interiors',
        'exteriores' => 'exteriors',
        'conceito'  => 'concept',
        'projetos'   => 'projects',
        'politica'   => 'Privacy Policy'
    ],

    'catalogo' => [
        'busca'       => 'search (code)',
        'resultado'   => 'search result',
        'encontrado'  => 'result found',
        'encontrados' => 'results found',
        'nenhum'      => 'no results found',
        'mais-opcoes' => 'more search options of',
        'ver-mais'    => 'see more',
        'dimensoes'   => 'Dimensions',
        'materiais'   => 'Materials',
        'adicionar'   => 'ADD THIS ITEM TO THE QUOTATION',
        'adicionado'  => 'ITEM ADDED TO THE QUOTATION REQUEST [REMOVE]',
    ],

    'blog' => [
        'nenhum'            => 'No posts found.',
        'anterior'          => 'newer posts',
        'proximo'           => 'older posts',
        'nenhum-comentario' => 'no comment',
        'comentario'        => 'comment',
        'comentarios'       => 'comments',
        'comente'           => 'please comment',
        'topo'              => 'top',
        'voltar'            => 'back',
        'nome'              => 'your name',
        'email'             => 'your e-mail',
        'mensagem'          => 'your comment',
        'enviar'            => 'send',
        'moderacao'         => 'All comments are moderated by Casual team members, who may choose not to display them, at their sole discretion. Therefore, the authors should not insist on the posting of rejected comments.',
        'enviado'           => 'Comment successfully sent! Waiting for approval.',
        'erro'              => 'Please, fill in all fields correctly.',
    ],

    'perfil' => [
        'visao'   => 'Vision',
        'valores' => 'Values',
        'missao'  => 'Mission',
    ],

    'contato' => [
        'fale-conosco' => 'CONTACT US',
        'nome'         => 'name',
        'telefone'     => 'phone',
        'mensagem'     => 'message',
        'enviar'       => 'SEND',
        'sucesso'      => 'Message successfully sent!',
        'erro'         => 'Please, fill in all fields correctly',
    ],

    'copyright' => 'All rights reserved',
    'criacao-de-sites' => 'Website Design',

    'texto-cookies-1' => 'We use cookies to personalize content, track ads and provide a safer browsing experience for you. By continuing to browse our website, you agree to our use of this information. read our ',
    'texto-cookies-2' => ' and learn more.',
    'btn-aceitar-cookies' => 'ACCEPT AND CLOSE',
];
