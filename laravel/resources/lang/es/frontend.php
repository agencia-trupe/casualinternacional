<?php

return [
    'pagina-nao-encontrada' => 'PÁGINA NO ENCONTRADA',

    'nav' => [
        'produtos'   => 'Productos',
        'marcas'     => 'Marcas',
        'novidades'  => 'Novedad',
        'perfil'     => 'Perfil',
        'designers'  => 'Designers',
        'contato'    => 'Contacto',
        'catalogo'   => 'Zona Profesional',
        'interiores' => 'interiores',
        'exteriores' => 'exteriores',
        'conceito'   => 'concepto',
        'projetos'   => 'proyectos',
        'politica'   => 'Política de Privacidad'
    ],

    'catalogo' => [
        'busca'       => 'buscar (código)',
        'resultado'   => 'resultado da busca',
        'encontrado'  => 'resultado encontrado',
        'encontrados' => 'resultados encontrados',
        'nenhum'      => '0 resultados',
        'mais-opcoes' => 'más opciones de',
        'ver-mais'    => 'ver más',
        'dimensoes'   => 'Dimension',
        'materiais'   => 'Materiales',
        'adicionar'   => 'AÑADIR ESTE ITEM A PEDIDO DE PRESUPUESTO',
        'adicionado'  => 'ITEM AÑADIDO A PEDIDO DE PRESUPUESTO [ELIMINAR]',
    ],

    'blog' => [
        'nenhum'            => 'No se han encontrado publicaciones.',
        'anterior'          => 'más reciente',
        'proximo'           => 'más antiguo',
        'nenhum-comentario' => 'No hay comentarios',
        'comentario'        => 'comentario',
        'comentarios'       => 'comentarios',
        'comente'           => 'comente',
        'topo'              => 'superior de la página',
        'voltar'            => 'volver',
        'nome'              => 'nombre',
        'email'             => 'e-mail',
        'mensagem'          => 'comentario',
        'enviar'            => 'enviar',
        'moderacao'         => 'Los comentarios son moderados por el equipo Casual, que puede optar por no mostrarlos si lo considera conveniente, y el comentarista no se hace responsable de ningún reclamo.',
        'enviado'           => '¡Comentario enviado con éxito! Esperando aprobación.',
        'erro'              => 'Rellene todos los campos correctamente.',
    ],

    'perfil' => [
        'visao'   => 'Vision',
        'valores' => 'Valores',
        'missao'  => 'Mision',
    ],

    'contato' => [
        'fale-conosco' => 'CONTACTO',
        'nome'         => 'nombre',
        'telefone'     => 'telefono',
        'mensagem'     => 'mensage',
        'enviar'       => 'ENVIAR',
        'sucesso'      => '¡Mensaje enviado correctamente!',
        'erro'         => 'Rellene todos los campos correctamente',
    ],

    'copyright' => 'Todos los derechos reservados',
    'criacao-de-sites' => 'Creación de website',

    'texto-cookies-1' => 'Usamos cookies para personalizar contenido, rastrear anuncios y brindarle una experiencia de navegación más segura. Si continúa navegando por nuestro sitio, acepta nuestro uso de esta información. Lea nuestra ',
    'texto-cookies-2' => ' y obtenga más información.',
    'btn-aceitar-cookies' => 'ACEPTAR Y CERRAR',
];
