    <footer>
        <div class="center">
            <div class="main">
                <nav>
                    <a href="#">{{ t('nav.produtos') }}</a>
                    <!-- <a href="{{ route('marcas') }}">{{ t('nav.marcas') }}</a> -->
                    <a href="{{ route('blog') }}">{{ t('nav.novidades') }}</a>
                    <a href="{{ route('perfil') }}">{{ t('nav.perfil') }}</a>
                    <a href="{{ route('designers') }}">{{ t('nav.designers') }}</a>
                    <a href="{{ route('contato') }}">{{ t('nav.contato') }}</a>
                </nav>

                <div class="tipos">
                    @foreach($tipos as $tipo)
                    <a href="{{ route('produtos.tipo', $tipo->slug_en) }}">{{ tobj($tipo, 'titulo') }}</a>
                    @endforeach
                </div>

                <div class="info">
                    <img src="{{ asset('assets/img/layout/marca-casual-en-internas.svg') }}" alt="">
                    <p>{{ $contato->telefone }}</p>
                    <a href="{{ route('home') }}" class="link-home">HOME</a>
                    <a href="{{ route('politica-de-privacidade') }}" class="link-politica">{{ t('nav.politica') }}</a>
                    <div class="social">
                        @foreach(['facebook', 'instagram', 'pinterest', 'twitter'] as $s)
                        @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }} - {{ t('copyright')}}
                    <span>|</span>
                    <a href="https://www.trupe.net">{{ t('criacao-de-sites') }}</a>:
                    <a href="https://www.trupe.net">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
