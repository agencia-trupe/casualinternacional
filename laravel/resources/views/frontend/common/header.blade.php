    <header @if(Tools::routeIs('home')) class="header-home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                @if(Tools::routeIs('home'))
                    <img src="{{ asset('assets/img/layout/marca-casual-en-home.svg') }}" alt="">
                @else
                    <img src="{{ asset('assets/img/layout/marca-casual-en-internas.svg') }}" alt="">
                @endif
            </a>
            <a href="{{ route('home') }}" class="link-home"></a>
            <nav id="nav-desktop">
                <div class="dropdown-handle @if(Tools::routeIs('produtos.*')) active @endif">
                    <span>{{ t('nav.produtos') }}</span>
                    <div class="dropdown dropdown-tipos">
                        <div class="center">
                            <div class="tipos">
                                @foreach($tipos->chunk(round(count($tipos) / 4)) as $chunk)
                                <div>
                                    @foreach($chunk as $tipo)
                                    <a href="{{ route('produtos.tipo', $tipo->slug_en) }}" @if(Route::currentRouteName() == 'produtos.tipo' && Route::current()->parameter('tipo_slug_en') == $tipo->slug_en) class="active" @endif>{{ tobj($tipo, 'titulo') }}</a>
                                    @endforeach
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <a href="{{ route('marcas') }}" @if(Tools::routeIs('marcas')) class="active" @endif>
                    <span>{{ t('nav.marcas') }}</span>
                </a> -->
                <a href="{{ route('blog') }}" @if(Tools::routeIs('blog*')) class="active" @endif>
                    <span>{{ t('nav.novidades') }}</span>
                </a>
                <a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil')) class="active" @endif>
                    <span>{{ t('nav.perfil') }}</span>
                </a>
                <a href="{{ route('designers') }}" @if(Tools::routeIs('designers*')) class="active" @endif>
                    <span>{{ t('nav.designers') }}</span>
                </a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
                    <span>{{ t('nav.contato') }}</span>
                </a>
            </nav>
            <div class="social @if(Tools::routeIs('home')) social-home @endif">
                @foreach(['facebook', 'instagram', 'pinterest', 'twitter'] as $s)
                @if($contato->{$s})
                <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                @endif
                @endforeach
            </div>

            <div class="handle-busca">
                @if(Tools::routeIs('home'))
                <img src="{{ asset('assets/img/layout/lupa-busca.svg') }}" alt="">
                @else
                <img src="{{ asset('assets/img/layout/icone-lupa.png') }}" alt="">
                @endif
            </div>

            @if(app()->getLocale() == 'es')
            <a href="{{ route('lang', 'en') }}" class="lang">
                <img src="{{ asset('assets/img/layout/lang-en.png') }}" alt="">
            </a>
            @else
            <a href="{{ route('lang', 'es') }}" class="lang">
                <img src="{{ asset('assets/img/layout/lang-es.png') }}" alt="">
            </a>
            @endif
            <button id="mobile-toggle">
                <div class="lines"></div>
            </button>
        </div>
    </header>

    <div id="nav-mobile">
        <div class="center">
            <div class="dropdown-mobile @if(Tools::routeIs('produtos.*')) active @endif">{{ t('nav.produtos') }}</div>
            <div class="dropdown-mobile-content">
                @foreach($tipos as $tipo)
                <a href="{{ route('produtos.tipo', $tipo->slug_en) }}" @if(Route::currentRouteName() == 'produtos.tipo' && Route::current()->parameter('tipo_slug_en') == $tipo->slug_en) class="active" @endif>{{ tobj($tipo, 'titulo') }}</a>
                @endforeach
            </div>
            <a href="{{ route('marcas') }}" @if(Tools::routeIs('marcas')) class="active" @endif>
                {{ t('nav.marcas') }}
            </a>
            <a href="{{ route('blog') }}" @if(Tools::routeIs('blog*')) class="active" @endif>{{ t('nav.novidades') }}</a>
            <a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil')) class="active" @endif>
                {{ t('nav.perfil') }}
            </a>
            <a href="{{ route('designers') }}" @if(Tools::routeIs('designers*')) class="active" @endif>
                {{ t('nav.designers') }}
            </a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
                {{ t('nav.contato') }}
            </a>
        </div>
    </div>

    <div id="busca-header">
        <div class="center">
            <form action="{{ route('produtos.busca') }}" method="GET">
                <div class="filtros">
                    @foreach(divisoes() as $divisaoEn => $divisaoPt)
                    <label class="radio">
                        <input type="radio" name="division" value="{{ $divisaoEn }}">
                        <div class="custom-radio"></div>
                        <span>{{ t('nav.'.$divisaoPt) }}</span>
                    </label>
                    @endforeach
                </div>
                <input type="text" name="code" placeholder="{{ t('catalogo.busca') }}">
                <button type="submit"></button>
            </form>
        </div>
    </div>
