@extends('frontend.common.template')

@section('content')

    <div class="blog">
        <div class="center">
            @include('frontend.blog._aside')

            <main>
                @if(count($posts))
                    @foreach($posts as $post)
                    <a href="{{ route('blog.show', $post->slug_en) }}" class="post">
                        <h2>{{ tobj($post, 'titulo') }}</h2>
                        <h3>
                            <span>
                                {{ Tools::dataBlog($post->data) }}
                                &middot;
                                {{ tobj($post->categoria, 'titulo') }}
                            </span>
                            <span>
                                @if(count($post->comentariosAprovados) == 0)
                                {{ t('blog.nenhum-comentario') }}
                                @else
                                {{ count($post->comentariosAprovados) }}
                                {{
                                    count($post->comentariosAprovados) > 1
                                        ? t('blog.comentarios')
                                        : t('blog.comentario')
                                }}
                                @endif
                                &middot;
                                {{ t('blog.comente') }}
                            </span>
                        </h3>
                        <img src="{{ casual_public('assets/images/blog/'.$post->capa) }}" alt="">
                    </a>
                    @endforeach

                    <div>
                        <a href="#" class="link-topo">{{ t('blog.topo') }}</a>
                    </div>
                    <div class="pagination">
                        @if($posts->previousPageUrl())
                            <a href="{{ $posts->previousPageUrl() }}" class="prev">{{ t('blog.anterior') }}</a>
                        @endif
                        @if($posts->nextPageUrl())
                            <a href="{{ $posts->nextPageUrl() }}" class="next">{{ t('blog.proximo') }}</a>
                        @endif
                    </div>
                @else
                    <p class="nenhum">{{ t('blog.nenhum') }}</p>
                @endif
            </main>
        </div>
    </div>

@endsection
