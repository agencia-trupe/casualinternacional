<aside>
    <div class="categorias">
        @foreach($categorias as $c)
            <a href="{{ route('blog', $c->slug_en) }}" @if(isset($categoria) && $categoria->slug_en == $c->slug_en) class="active" @endif>
                {{ tobj($c, 'titulo') }}
            </a>
        @endforeach
    </div>

    <div class="anos">
        @foreach($anos as $a)
            <a href="{{ route('blog.data', $a->ano) }}" @if(isset($ano) && $ano == $a->ano) class="active" @endif>
                {{ $a->ano }}
            </a>

            @if(isset($ano) && $ano == $a->ano)
                <div class="meses">
                    @foreach($meses as $m)
                    <a href="{{ route('blog.data', [$a->ano, $m->mes]) }}" @if(isset($mes) && $mes == $m->mes) class="active" @endif>
                        [{{ Tools::mesExtenso((int) $m->mes) }}]
                    </a>
                    @endforeach
                </div>
            @endif
        @endforeach
    </div>

    <div class="tags">
        <h2>TAGS</h2>
        @foreach($tags as $t)
            <a href="{{ route('blog.tag', $t->slug_en) }}" @if(isset($tag) && $tag->slug_en == $t->slug_en) class="active" @endif>
                {{ $t->titulo_en }}
            </a>
        @endforeach
    </div>
</aside>
