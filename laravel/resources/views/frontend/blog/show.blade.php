@extends('frontend.common.template')

@section('content')

    <div class="blog">
        <div class="center">
            @include('frontend.blog._aside')

            <main>
                <div class="post">
                    <h2>{{ tobj($post, 'titulo') }}</h2>
                    <h3>
                        <span>
                            {{ Tools::dataBlog($post->data) }}
                            &middot;
                            {{ tobj($post->categoria, 'titulo') }}
                        </span>
                        <span>
                            @if(count($post->comentariosAprovados) == 0)
                            {{ t('blog.nenhum-comentario') }}
                            @else
                            {{ count($post->comentariosAprovados) }}
                            {{
                                count($post->comentariosAprovados) > 1
                                    ? t('blog.comentarios')
                                    : t('blog.comentario')
                            }}
                            @endif
                            &middot;
                            {{ t('blog.comente') }}
                        </span>
                    </h3>
                    <img src="{{ casual_public('assets/images/blog/'.$post->capa) }}" alt="">

                    <div class="post-content">
                        {!! tobj($post, 'texto') !!}
                    </div>

                    <div class="post-tags">
                        @foreach($post->tags_en as $tag)
                        <a href="{{ route('blog.tag', $tag->slug_en) }}">
                            {{ $tag->titulo_en }}
                        </a>
                        @endforeach
                    </div>

                    <div class="post-comentarios">
                        <form action="{{ route('blog.comentario', $post->slug_en) }}" method="POST">
                            {!! csrf_field() !!}

                            <h3>{{ t('blog.comentarios') }} [{{ count($post->comentariosAprovados) }}]</h3>

                            @if($errors->any())
                                <p class="enviado">{{ t('blog.erro') }}</p>
                            @endif

                            @if(session('comentarioEnviado'))
                                <p class="enviado">{{ t('blog.enviado') }}</p>
                            @else
                                <input type="text" name="autor" placeholder="{{ t('blog.nome') }}" value="{{ old('autor') }}" required>
                                <input type="email" name="email" placeholder="{{ t('blog.email') }}" value="{{ old('email') }}" required>
                                <input type="text" name="comentario" placeholder="{{ t('blog.mensagem') }}" value="{{ old('comentario') }}" required>
                                <input type="submit" value={{ t('blog.enviar') }}>

                                <p>{{ t('blog.moderacao') }}</p>
                            @endif
                        </form>

                        <div class="comentarios">
                            @foreach($post->comentariosAprovados as $comentario)
                            <div class="comentario">
                                {{ Tools::dataBlog($comentario->created_at->format('d/m/Y')) }}
                                {{ $comentario->autor }}<br>
                                {{ $comentario->texto }}
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <a href="#" class="link-topo">{{ t('blog.topo') }}</a>
                    <div class="pagination">
                        <a href="{{ url()->previous() }}" class="prev">{{ t('blog.voltar') }}</a>
                    </div>
                </div>
            </main>
        </div>
    </div>

@endsection
