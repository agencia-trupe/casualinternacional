@extends('frontend.common.template')

@section('content')

    <div class="marcas">
        <div class="center">
            <div class="marcas-header">
                <h1>
                    <a href="{{ route('marcas') }}">
                        {{ t('nav.marcas') }}
                    </a>
                </h1>

                <div class="filtros">
                    @foreach(divisoes() as $divisao)
                    <label class="radio">
                        <input type="radio" name="divisao" value="{{ $divisao }}" data-route="{{ route('marcas', ['divisao' => $divisao]) }}" class="radio-route" @if(request('divisao') === $divisao) checked @endif>
                        <div class="custom-radio"></div>
                        <span>{{ t('nav.'.$divisao) }}</span>
                    </label>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="marcas-grid">
            @foreach($marcas as $marca)
            <div class="marca" title="{{ $marca->nome }}">
                <img src="{{ casual_public('assets/img/marcas/'.$marca->imagem) }}" alt="">
            </div>
            @endforeach
        </div>
    </div>

@endsection
