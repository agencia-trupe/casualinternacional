@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
            @if($banner->link)
            <a href="{{ $banner->link }}" class="slide" style="background-image:url('{{ casual_public('assets/img/internacional/banners/'.$banner->imagem) }}')"></a>
            @else
            <div class="slide" style="background-image:url('{{ casual_public('assets/img/internacional/banners/'.$banner->imagem) }}')"></div>
            @endif
        @endforeach
    </div>

    <div class="home">
        @if($home->link)
            <a href="{{ Tools::parseLink($home->link) }}" class="imagem" style="background-image:url('{{ casual_public('assets/img/internacional/home/'.$home->imagem) }}')">
                <span>{{ tobj($home, 'titulo') }}</span>
            </a>
        @else
            <div class="imagem" style="background-image:url('{{ casual_public('assets/img/internacional/home/'.$home->imagem) }}')">
                <span>{{ tobj($home, 'titulo') }}</span>
            </div>
        @endif

        <div class="chamadas">
            @foreach($chamadas as $chamada)
            <a href="{{ $chamada->link }}" class="chamada" style="background-image:url('{{ casual_public('assets/img/chamadas/'.$chamada->imagem) }}')">
                <div class="texto">
                    <span>{{ tobj($chamada, 'subtitulo') }}</span>
                    <p>{{ tobj($chamada, 'titulo') }}</p>
                </div>
            </a>
            @endforeach
            <div class="cycle-pager"></div>
        </div>

        @if($home->link_2)
            <a href="{{ Tools::parseLink($home->link_2) }}" class="imagem" style="background-image:url('{{ casual_public('assets/img/internacional/home/'.$home->imagem_2) }}')">
                <span>{{ tobj($home, 'titulo_2') }}</span>
            </a>
        @else
            <div class="imagem" style="background-image:url('{{ casual_public('assets/img/internacional/home/'.$home->imagem_2) }}')">
                <span>{{ tobj($home, 'titulo_2') }}</span>
            </div>
        @endif
    </div>

@endsection
