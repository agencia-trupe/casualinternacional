@extends('frontend.common.template')

@section('content')

    <div class="designers">
        <p>{{ tobj($frase, 'frase') }}</p>

        <div class="designers-thumbs">
            @foreach($designers as $designer)
            <a href="{{ route('designers.show', $designer->slug) }}">
                <img src="{{ casual_public('assets/img/designers/'.$designer->foto) }}" alt="">
                <span>{{ $designer->nome }}</span>
            </a>
            @endforeach
        </div>
    </div>

@endsection
