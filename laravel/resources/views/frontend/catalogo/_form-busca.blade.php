<div class="form-busca">
    @if(isset($tipo))
    <div class="filtros">
        @foreach(divisoes() as $divisaoEn => $divisaoPt)
        <label class="radio">
            <input type="radio" name="divisao" value="{{ $divisaoEn }}" class="radio-route" data-route="{{ route('produtos.tipo', ['tipo' => $tipo->slug_en, 'codigo' => null, 'division' => $divisaoEn]) }}" @if(request('divisao') === $divisaoEn) checked @endif>
            <div class="custom-radio"></div>
            <span>{{ t('nav.'.$divisaoPt) }}</span>
        </label>
        @endforeach
    </div>
    @endif
    <form action="{{ route('produtos.busca') }}" method="GET">
        @if(!isset($tipo))
        <div class="filtros">
            @foreach(divisoes() as $divisaoEn => $divisaoPt)
            <label class="radio">
                <input type="radio" name="division" value="{{ $divisaoEn }}" @if(request('division') === $divisaoEn) checked @endif>
                <div class="custom-radio"></div>
                <span>{{ t('nav.'.$divisaoPt) }}</span>
            </label>
            @endforeach
        </div>
        @else
        <input type="radio" name="division" value="{{ $divisaoEn }}" @if(request('division') === $divisaoPt) checked @endif>
        @endif
        <input type="text" name="code" placeholder="{{ t('catalogo.busca') }}" value="{{ request('code') }}" required>
        <button type="submit"></button>
    </form>
</div>
