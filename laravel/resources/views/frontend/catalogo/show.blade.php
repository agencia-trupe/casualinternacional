@extends('frontend.common.template')

@section('content')

<div class="catalogo catalogo-index">
    <div class="center">
        <div class="top-bar">
            <h2>
                {{ t('nav.produtos') }} &middot;
                {{ tobj($tipo, 'titulo') }}
            </h2>

            @include('frontend.catalogo._form-busca')
        </div>
    </div>

    <div class="produto center">
        <div class="left">
            <img src="{{ casual_public('assets/images/produtos/redimensionadas/'.$produto->imagem_capa) }}" alt="">
            @if($produto->item)
            <h1>{{ tobj($produto, 'item') }} • {{ $produto->codigo }}</h1>
            @else
            <h1>{{ $produto->codigo }}</h1>
            @endif
            <p>
                @foreach(explode(',', $produto->divisao) as $divisao)
                <span>{{ t('nav.'.$divisao) }}</span>
                @endforeach
            </p>

            @if($produto->exibir_3d)
            <div id="container-3d"></div>
            <script>
                var $3dViewer = document.getElementById('container-3d');

                R2U.init({
                    customerId: '{{ env('
                    R2U_CUSTOMER_ID ') }}'
                }).then((data) => {
                    R2U.sku.isActive('{{ $produto->codigo }}').then((isActive) => {
                        if (!isActive) {
                            return $3dViewer.remove();
                        }

                        R2U.viewer.create({
                            element: $3dViewer,
                            sku: '{{ $produto->codigo }}',
                            name: '{{ tobj($produto, '
                            titulo ') }}',
                            popup: false,
                        });
                    }).catch((err) => {
                        $3dViewer.remove();
                    });
                });
            </script>
            @endif
        </div>
        <div class="right">
            @foreach($produto->imagens as $imagem)
            <img src="{{ casual_public('assets/images/produtos/redimensionadas/'.$imagem->imagem) }}" alt="">
            @endforeach
        </div>
    </div>

    @if(count($produtos) > 1)
    <div class="mais-opcoes">
        <div class="center">
            <h3>
                {{ t('catalogo.mais-opcoes') }}
                {{ tobj($tipo, 'titulo') }}
            </h3>
        </div>
    </div>

    <div class="produtos produtos-small">
        @foreach($produtos->filter(function($o) use ($produto) { return $o->id !== $produto->id; })->shuffle()->chunk(48) as $page => $chunk)
        @foreach($chunk as $p)
        <a href="{{ route('produtos.tipo', [$tipo->slug_en, $p->codigo]) }}" data-page="{{ $page }}" @if($page===0) class="visible" @endif>
            <div class="imagem" style="background-image:url('{{ casual_public('assets/images/produtos/capas/'.$p->imagem_capa) }}')"></div>
            <div class="overlay">
                <span>{{ $p->codigo }}</span>
            </div>
            {{ tobj($p, 'item') }}
        </a>
        @endforeach
        @endforeach
    </div>
    @endif

    <a href="#" class="btn-ver-mais" style="display: none">
        {{ t('catalogo.ver-mais') }} +
    </a>
</div>

@endsection