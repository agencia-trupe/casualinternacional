@extends('frontend.common.template')

@section('content')

<div class="catalogo catalogo-index">
    <div class="center">
        <div class="top-bar">
            <h2>
                {{ t('nav.produtos') }} &middot;
                {{ tobj($tipo, 'titulo') }}
            </h2>

            @include('frontend.catalogo._form-busca')
        </div>
    </div>

    @if(!count($produtos))
    <div class="center">
        <p class="resultados">
            {{ t('catalogo.nenhum') }}
        </p>
    </div>
    @endif

    <div class="produtos">
        @foreach($produtos as $produto)
        <a href="{{ route('produtos.tipo', [$tipo->slug_en, $produto->codigo]) }}">
            <div class="imagem" style="background-image:url('{{ casual_public('assets/images/produtos/capas/'.$produto->imagem_capa) }}')"></div>
            <div class="overlay">
                <span>
                    @if($produto->item)
                    {{ tobj($produto, 'item') }} • {{ $produto->codigo }}
                    @else
                    {{ $produto->codigo }}
                    @endif
                </span>
            </div>
        </a>
        @endforeach
    </div>
</div>

@endsection