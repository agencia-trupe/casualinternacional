@extends('frontend.common.template')

@section('content')

    <div class="not-found">
        <h1>{{ t('pagina-nao-encontrada') }}</h1>
    </div>

@endsection
