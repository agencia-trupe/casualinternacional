import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(".banners").cycle({
  slides: ">.slide",
});

$(".chamadas").cycle({
  slides: ">.chamada",
});

$(".fancybox").fancybox();

$(".link-topo").click(function (event) {
  event.preventDefault();

  $("html, body").animate(
    {
      scrollTop: 0,
    },
    "slow"
  );
});

if ($(".post-comentarios .enviado").length) {
  $("html, body").animate(
    {
      scrollTop: $(".post-comentarios").offset().top - 30,
    },
    "slow"
  );
}

$(".dropdown-mobile").click(function () {
  $(this).toggleClass("open").next().slideToggle();
});

$(".contato .enderecos nav a").click(function (event) {
  event.preventDefault();

  const $this = $(this);
  const $mapa = $(".contato .enderecos .mapa");

  if ($this.hasClass("active")) return;

  const selectedMap = $this.data("mapa");

  $this.parent().find("a").removeClass("active");
  $this.addClass("active");

  $mapa.html(selectedMap);

  $("html, body").animate(
    {
      scrollTop: $mapa.offset().top,
    },
    "fast"
  );
});

$(".radio-route").change(function (event) {
  window.location = $(this).data("route");
});

$(".handle-busca").click(function () {
  const $handle = $(this);
  const $busca = $("#busca-header");

  $handle.toggleClass("active");
  $busca.fadeToggle();
});

(function () {
  const $produtosSmall = $(".produtos-small");

  if (!$produtosSmall.length) return;

  let page = 0;
  const $seeMore = $(".btn-ver-mais");

  const checkHasMore = () => {
    if ($produtosSmall.find(`a[data-page=${page + 1}]`).length) {
      return $seeMore.show();
    }

    return $seeMore.hide();
  };

  checkHasMore();

  $seeMore.click(function (event) {
    event.preventDefault();

    page += 1;

    $produtosSmall.find(`a[data-page=${page}]`).addClass("visible");
    checkHasMore();
  });
})();

$(document).ready(function () {
  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  $(".aceitar-cookies").click(function () {
    var url = window.location.origin + "/cookie-acceptance";

    $.ajax({
      type: "POST",
      url: url,
      success: function (data, textStatus, jqXHR) {
        $(".aviso-cookies").hide();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  });
});
