<?php

    function t($termo, $params = []) {
        return trans('frontend.'.$termo, $params);
    }

    function tobj($obj, $termo)
    {
        $lang = app()->getLocale();

        if ($lang == 'pt') return $obj->{$termo};

        return $obj->{$termo.'_'.app()->getLocale()} ?: $obj->{$termo};
    }

    // function divisoes()
    // {
    //     return [
    //         'interiores',
    //         'exteriores',
    //         'conceito',
    //         'projetos',
    //     ];
    // }

    function divisoes()
    {
        return [
            'interiors' => 'interiores',
            'exteriors' => 'exteriores',
            'concept' => 'conceito',
            'projects' => 'projetos',
        ];
    }

    function casual_public($path)
    {
        return 'https://www.casualmoveis.com.br/'.$path;
    }
