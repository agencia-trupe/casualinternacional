<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('enderecos', 'App\Models\Endereco');
		$router->model('produtos', 'App\Models\Produto');
		$router->model('imagens_produtos', 'App\Models\ProdutoImagem');
		$router->model('linhas', 'App\Models\Linha');
		$router->model('fornecedores', 'App\Models\Fornecedor');
		$router->model('produtos-materiais', 'App\Models\ProdutoMaterial');
		$router->model('tipos', 'App\Models\Tipo');
		$router->model('categorias_blog', 'App\Models\BlogCategoria');
		$router->model('posts', 'App\Models\BlogPost');
		$router->model('tags', 'App\Models\BlogTag');
        $router->model('comentarios', 'App\Models\BlogComentario');

		$router->model('chamadas', 'App\Models\Chamada');
		$router->model('home', 'App\Models\Home');
		$router->model('banners', 'App\Models\Banner');
		$router->model('marcas', 'App\Models\Marca');
		$router->model('perfil', 'App\Models\Perfil');
		$router->model('designers-frase', 'App\Models\DesignersFrase');
		$router->model('designers', 'App\Models\Designer');
		$router->model('imagens_designers', 'App\Models\DesignerImagem');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('aceite-de-cookies', 'App\Models\AceiteDeCookies');

        $router->bind('designer_slug', function($value) {
            return \App\Models\Designer::whereSlug($value)->firstOrFail();
        });

        $router->bind('blog_categoria_slug', function($value) {
            return \App\Models\BlogCategoria::whereSlug($value)->firstOrFail();
        });
        $router->bind('blog_post_slug', function($value) {
            return \App\Models\BlogPost::whereSlug($value)->firstOrFail();
        });
        $router->bind('blog_tag_slug', function($value) {
            return \App\Models\BlogTag::whereSlug($value)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
