<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\IntConfiguracoes::first());
        });

        view()->composer('frontend.common.template', function ($view) {
            $view->with('contato', \App\Models\IntContato::first());
            $view->with('tipos', \App\Models\Tipo::whereHas('produtos', function ($q) {
                $q->internacional()->site();
            })->ordenados()->get());
        });

        view()->composer('frontend.*', function ($view) {
            $view->with('orcamento', session()->get('orcamento', []));
            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
