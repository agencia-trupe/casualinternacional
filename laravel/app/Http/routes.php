<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('brands', 'MarcasController@index')->name('marcas');
    Route::get('products/type/{tipo_slug_en}/{codigo?}', 'ProdutosController@tipo')->name('produtos.tipo');
    Route::get('products/search', 'ProdutosController@busca')->name('produtos.busca');
    Route::post('blog/comment/{blog_post_slug_en}', 'BlogController@comentario')->name('blog.comentario');
	Route::get('blog/{blog_categoria_slug_en?}', 'BlogController@index')->name('blog');
	Route::get('blog/year/{ano}/{mes?}', 'BlogController@data')->name('blog.data');
	Route::get('blog/post/{blog_post_slug_en?}', 'BlogController@show')->name('blog.show');
	Route::get('blog/tag/{blog_tag_slug_en?}', 'BlogController@tag')->name('blog.tag');
    Route::get('profile', 'PerfilController@index')->name('perfil');
    Route::get('designers', 'DesignersController@index')->name('designers');
    Route::get('designers/{designer_slug}', 'DesignersController@show')->name('designers.show');
    Route::get('contact', 'ContatoController@index')->name('contato');
    Route::post('contact', 'ContatoController@post')->name('contato.post');

    Route::get('privacy-policy', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('cookie-acceptance', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['en', 'es'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->intended();
    })->name('lang');
});
