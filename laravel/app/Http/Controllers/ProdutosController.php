<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Produto;
use App\Models\Tipo;
use Symfony\Component\Console\Helper\Helper;

class ProdutosController extends Controller
{
    private function divisoes()
    {
        return [
            'interiors' => 'interiores',
            'exteriors' => 'exteriores',
            'concept' => 'conceito',
            'projects' => 'projetos',
        ];
    }
    public function tipo($tipo_slug_en, $codigo = null)
    {
        $tipo = Tipo::where('slug_en', $tipo_slug_en)->first();
        $produtos = Produto::site()->internacional()->where('tipos_id', $tipo->id);

        if (request('division')) {
            $divisao = $this->divisoes()[request('division')];
            $produtos = $produtos->where('divisao', 'LIKE', "%$divisao%");
        }

        $produtos = $produtos->ordenados()->get();

        if (!$codigo) {
            return view('frontend.catalogo.index', compact('tipo', 'produtos'));
        }

        $produto = $produtos->where('codigo', $codigo)->first();

        if (!$produto) {
            abort('404');
        }

        return view('frontend.catalogo.show', compact('tipo', 'produtos', 'produto'));
    }

    public function busca(Request $request)
    {
        $produtos = Produto::site()->internacional()->has('tipo');

        if ($request->get('code')) {
            $codigo = $request->get('code');
            $produtos = $produtos->where('codigo', 'LIKE', "%$codigo%");
        } else {
            abort('404');
        }
        if ($request->get('division')) {
            $divisao = $this->divisoes()[$request->get('division')];
            $produtos = $produtos->where('divisao', 'LIKE', "%$divisao%");
        }

        $produtos = $produtos->get();

        return view('frontend.catalogo.busca', compact('produtos'));
    }
}
