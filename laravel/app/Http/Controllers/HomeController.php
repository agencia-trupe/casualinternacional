<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\Chamada;
use App\Models\Home;
use App\Models\IntBanner;
use App\Models\IntHome;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $banners  = IntBanner::ordenados()->get();
        $home     = IntHome::first();
        $chamadas = Chamada::internacional()->ordenados()->get();

        return view('frontend.home', compact('banners', 'home', 'chamadas'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect()->back();
    }
}
