<?php

namespace App\Http\Controllers;

use App\Models\Designer;
use App\Models\DesignersFrase;

class DesignersController extends Controller
{
    public function index()
    {
        $frase     = DesignersFrase::first();
        $designers = Designer::internacional()->ordenados()->get();

        return view('frontend.designers.index', compact('frase', 'designers'));
    }

    public function show(Designer $designer)
    {
        return view('frontend.designers.show', compact('designer'));
    }
}
