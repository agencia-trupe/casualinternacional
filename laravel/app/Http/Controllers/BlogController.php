<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogComentariosRequest;

use App\Models\BlogCategoria;
use App\Models\BlogPost;
use App\Models\BlogTag;
use App\Models\BlogTagEn;

class BlogController extends Controller
{
    private $postsPerPage = 5;

    private $categorias;
    private $tags;
    private $anos;

    public function __construct()
    {
        $this->categorias = BlogCategoria::whereHas('posts', function ($query) {
            $query->publicados()->internacional();
        })->get();

        $this->tags = BlogTagEn::whereHas('posts', function ($query) {
            $query->publicados()->internacional();
        })->ordenados()->get();

        $this->anos = BlogPost::publicados()->internacional()
            ->distinct()
            ->select(\DB::raw('YEAR(data) as ano'))
            ->groupBy('ano')
            ->orderBy('ano', 'DESC')
            ->get();
    }

    public function index($blog_categoria_slug_en = null)
    {
        $categoria = BlogCategoria::where('slug_en', $blog_categoria_slug_en)->first();
        
        $posts = isset($categoria)
            ? $categoria->posts()
            : BlogPost::query();

        $posts = $posts
            ->with('categoria', 'comentariosAprovados', 'tags_en')
            ->publicados()
            ->internacional()
            ->ordenados()
            ->paginate($this->postsPerPage);
        
        return view('frontend.blog.index', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'categoria'  => $categoria,
            'posts'      => $posts,
        ]);
    }

    public function data($ano, $mes = null)
    {
        $meses = BlogPost::publicados()->internacional()
            ->distinct()
            ->select(\DB::raw('MONTH(data) as mes'))
            ->groupBy('mes')
            ->orderBy('mes', 'DESC')
            ->whereRaw("YEAR(data) = $ano")
            ->get();

        $posts = BlogPost::with('categoria', 'comentariosAprovados', 'tags')
            ->publicados()
            ->internacional()
            ->ordenados();

        if ($mes) {
            $posts = $posts->whereRaw("YEAR(data) = $ano AND MONTH(data) = $mes");
        } else {
            $posts = $posts->whereRaw("YEAR(data) = $ano");
        }

        $posts = $posts->paginate($this->postsPerPage);

        return view('frontend.blog.index', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'meses'      => $meses,
            'posts'      => $posts,
            'ano'        => $ano,
            'mes'        => $mes
        ]);
    }

    public function tag($blog_tag_slug_en)
    {
        $tag = BlogTagEn::where('slug_en', $blog_tag_slug_en)->first();
        
        $posts = BlogPost::with('categoria', 'comentariosAprovados', 'tags_en')
            ->publicados()
            ->internacional()
            ->ordenados()
            ->whereHas('tags_en', function ($query) use ($tag) {
                $query->where('slug_en', $tag->slug_en);
            })
            ->paginate($this->postsPerPage);

        return view('frontend.blog.index', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'tag'        => $tag,
            'posts'      => $posts,
        ]);
    }

    public function show($blog_post_slug_en)
    {
        $post = BlogPost::where('slug_en', $blog_post_slug_en)->first();

        return view('frontend.blog.show', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'post'       => $post,
            'categoria'  => $post->categoria,
        ]);
    }

    public function comentario(BlogComentariosRequest $request, $blog_post_slug_en)
    {
        $post = BlogPost::where('slug_en', $blog_post_slug_en)->first();

        $post->comentarios()->create([
            'autor'    => request('autor'),
            'email'    => request('email'),
            'texto'    => request('comentario'),
            'data'     => Date('Y-m-d'),
            'aprovado' => false
        ]);

        return back()->with('comentarioEnviado', true);
    }
}
