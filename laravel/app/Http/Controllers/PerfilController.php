<?php

namespace App\Http\Controllers;

use App\Models\IntPerfil;
use App\Models\Perfil;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = IntPerfil::first();

        return view('frontend.perfil', compact('perfil'));
    }
}
