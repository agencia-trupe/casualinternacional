<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Marca;

class MarcasController extends Controller
{
    public function index(Request $request)
    {
        if ($divisao = $request->divisao) {
            $marcas = Marca::where('divisao', 'LIKE', "%$divisao%")
                ->internacional()->ordenados()->get();
        } else {
            $marcas = Marca::internacional()->ordenados()->get();
        }

        return view('frontend.marcas', compact('marcas'));
    }
}
