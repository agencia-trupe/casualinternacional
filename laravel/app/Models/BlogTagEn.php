<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogTagEn extends Model
{
    protected $table = 'blog_tags_en';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('titulo_en', 'ASC');
    }

    public function posts()
    {
        return $this->belongsToMany(BlogPost::class, 'blog_tags_en_has_blog_posts', 'blog_tags_en_id', 'blog_posts_id');
    }
}
