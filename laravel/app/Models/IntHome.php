<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class IntHome extends Model
{
    protected $table = 'int_home';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 800,
            'height' => 512,
            'path'   => 'assets/img/internacional/home/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 800,
            'height' => 512,
            'path'   => 'assets/img/internacional/home/'
        ]);
    }
}
