<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function scopeSite($query)
    {
        return $query->whereHas('fornecedor', function ($q) {
            $q->where('publicar_site', '=', 1);
        })->where('publicar_site', '=', 1);
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedores_id');
    }

    public function linha()
    {
        return $this->belongsTo(Linha::class, 'linhas_id');
    }

    public function imagens()
    {
        return $this->hasMany(ProdutoImagem::class, 'produtos_id')->ordenados();
    }

    public function materiais()
    {
        return $this->belongsToMany(ProdutoMaterial::class, 'produtos_has_produtos_materiais', 'produtos_id', 'produtos_materiais_id');
    }

    public function tipo()
    {
        return $this->belongsTo(Tipo::class, 'tipos_id');
    }

    public function scopeBrasil($query)
    {
        return $query->where('brasil', '=', 1);
    }

    public function scopeInternacional($query)
    {
        return $query->where('internacional', '=', 1);
    }

    public static function upload_imagem_capa()
    {
        return CropImage::make('imagem_capa', [
            [
                'width'   => 220,
                'height'  => 220,
                'path'    => 'assets/images/produtos/capas/'
            ],
            [
                'width'   => null,
                'height'  => null,
                'path'    => 'assets/images/produtos/originais/'
            ],
            [
                'width'   => 890,
                'height'  => null,
                'path'    => 'assets/images/produtos/redimensionadas/'
            ],
            [
                'width'   => null,
                'height'  => 120,
                'path'    => 'assets/images/produtos/catalogo/'
            ]
        ]);
    }
}
