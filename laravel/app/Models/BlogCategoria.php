<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class BlogCategoria extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'blog_categorias';

    protected $guarded = ['id'];

    public function posts()
    {
        return $this->hasMany(BlogPost::class, 'blog_categorias_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }
}
