<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Tipo extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'tipos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function scopeBrasil($query)
    {
        return $query->where('brasil', '=', 1);
    }

    public function scopeInternacional($query)
    {
        return $query->where('internacional', '=', 1);
    }

    public function produtos()
    {
    	return $this->hasMany(Produto::class, 'tipos_id');
    }
}
