<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;
use App\Helpers\CropImage;

class BlogPost extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'blog_posts';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopePublicados($query)
    {
        return $query->where('publicar', '=', 1);
    }

    public function scopeBrasil($query)
    {
        return $query->where('brasil', '=', 1);
    }

    public function scopeInternacional($query)
    {
        return $query->where('internacional', '=', 1);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function categoria()
    {
        return $this->belongsTo(BlogCategoria::class, 'blog_categorias_id');
    }

    public function tags()
    {
        return $this->belongsToMany(BlogTag::class, 'blog_tags_has_blog_posts', 'blog_posts_id', 'blog_tags_id');
    }

    public function tags_en()
    {
        return $this->belongsToMany(BlogTagEn::class, 'blog_tags_en_has_blog_posts', 'blog_posts_id', 'blog_tags_en_id');
    }

    public function comentarios()
    {
        return $this->hasMany(BlogComentario::class, 'blog_posts_id')->ordenados();
    }

    public function comentariosAprovados()
    {
        return $this->hasMany(BlogComentario::class, 'blog_posts_id')
            ->where('aprovado', 1)
            ->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/images/blog/',
            'upsize' => true,
        ]);
    }
}
