<?php

namespace App\Helpers;

use Carbon\Carbon;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (str_is($routeName, \Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function fileUpload($input, $path = 'arquivos/')
    {
        if (! $file = request()->file($input)) {
            throw new \Exception("O campo (${input}) não contém nenhum arquivo.", 1);
        }

        $fileName  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_'.date('YmdHis');
        $fileName .= str_random(10);
        $fileName .= '.'.$file->getClientOriginalExtension();

        $file->move(public_path($path), $fileName);

        return $fileName;
    }

    public static function parseLink($url)
    {
        return parse_url($url, PHP_URL_SCHEME) === null ? 'http://'.$url : $url;
    }

    public static function mesExtenso($mes)
    {
        $meses = [
            'en' => ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
            'es' => ['enero', 'febrero', 'marcha', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']
        ];

        return $meses[app()->getLocale()][$mes - 1];
    }

    public static function dataBlog($data)
    {
        $d = Carbon::createFromFormat('d/m/Y', $data);

        return
            $d->format('d').' '.
            substr(mb_strtoupper(self::mesExtenso($d->format('m'))), 0, 3).' '.
            $d->format('Y');
    }

    public static function listaPaises()
    {
        $paises = [
            ['Afganistán', 'Afghanistan'],
            ['Andorra', 'Andorra'],
            ['Angola', 'Angola'],
            ['Antigua y Barbuda', 'Antigua e Barbuda'],
            ['Argelia', 'Algeria'],
            ['Argentina', 'Argentina'],
            ['Armenia', 'Armenia'],
            ['Australia', 'Australia'],
            ['Austria', 'Austria'],
            ['Azerbaiyán', 'Azerbaijan'],
            ['Bahamas', 'The Bahamas'],
            ['Bangladesh', 'Bangladesh'],
            ['Barbados', 'Barbados'],
            ['Bahréin', 'Bahrain'],
            ['Bielorrusia', 'Belarus'],
            ['Bélgica', 'Belgium'],
            ['Belice', 'Belize'],
            ['Benim', 'Benin'],
            ['Bolivia', 'Bolivia'],
            ['Bosnia; Bosnia y Herzegovina', 'Bosnia; Bosnia and Herzegovina'],
            ['Botswana', 'Botswana'],
            ['Brasil', 'Brazil'],
            ['Brunei', 'Brunei'],
            ['Bulgaria', 'Bulgaria'],
            ['BurkinaFaso', 'BurkinaFaso'],
            ['Burundi', 'Burundi'],
            ['Bután', 'Bhutan'],
            ['Cabo Verde', 'Cape Verde'],
            ['Camerún', 'Cameroon'],
            ['Camboya', 'Cambodia'],
            ['Canadá', 'Canada'],
            ['República Centro-Africana', 'Central African Republic'],
            ['Chad', 'Chad'],
            ['China', 'China'],
            ['Chile', 'Chile'],
            ['Islas Cook', 'Cook Islands'],
            ['Colombia', 'Colombia'],
            ['Comoras', 'Comoros'],
            ['Costa Rica', 'Costa Rica'],
            ['Croacia', 'Croatia'],
            ['Cuba', 'Cuba'],
            ['Chipre', 'Cyprus'],
            ['República Tcheca', 'Czech Republic'],
            ['República Democrática del Congo', 'Democratic Republic of Congo'],
            ['Dinamarca', 'Denmark'],
            ['Djibouti', 'Djibouti'],
            ['Dominica', 'Dominica'],
            ['República Dominicana', 'Dominican Republic'],
            ['Timor Oriental', 'East Timor'],
            ['Ecuador', 'Ecuador'],
            ['Egipto', 'Egypt'],
            ['El Salvador', 'El Salvador'],
            ['Inglaterra', 'England'],
            ['Guinea Ecuatorial', 'Equatorial Guinea'],
            ['Eritreia', 'Eritrea'],
            ['Estonia', 'Estônia'],
            ['Fiji', 'Fiji'],
            ['Finlandia', 'Finland'],
            ['Francia', 'France'],
            ['Gabón', 'Gabon'],
            ['Gambia', 'Gambia'],
            ['Georgia', 'Georgia'],
            ['Alemania', 'Germany'],
            ['Granada', 'Grenada'],
            ['Grecia', 'Greece'],
            ['Guatemala', 'Guatemala'],
            ['Guinea', 'Guinea'],
            ['Guinea Bissau', 'Guinea–Bissau'],
            ['Guayana', 'Guyana'],
            ['Haití', 'Haiti'],
            ['Países Bajos', 'Holland'],
            ['Honduras', 'Honduras'],
            ['Hungría', 'Hungary'],
            ['Islandia', 'Iceland'],
            ['India', 'India'],
            ['Indonesia', 'Indonesia'],
            ['Iran', 'Iran'],
            ['Irlanda', 'Ireland'],
            ['Israel', 'Israel'],
            ['Italia', 'Italy'],
            ['Costa de Marfil', 'Ivory Coast'],
            ['Jamaica', 'Jamaica'],
            ['Japón', 'Japan'],
            ['Jordán', 'Jordan'],
            ['Kazajstán', 'Kazakhstan'],
            ['Kenia', 'Kenya'],
            ['Kiribati', 'Kiribati'],
            ['Kirguistán', 'Kyrgyzstan'],
            ['Kuwait', 'Kwait'],
            ['Laos', 'Laos'],
            ['Letonia', 'Latvia'],
            ['Líbano', 'Lebanon'],
            ['Lesoto', 'Lesotho'],
            ['Liberia', 'Liberia'],
            ['Liechtenstein', 'Liechtenstein'],
            ['Lituania', 'Lithuania'],
            ['Luxemburgo', 'Luxembourg'],
            ['Libia', 'Lybia'],
            ['Macedonia', 'Macedonia'],
            ['Madagascar', 'Madagascar'],
            ['Malasia', 'Malaysia'],
            ['Malawi', 'Malawi'],
            ['Maldivas', 'Maldives'],
            ['Mali', 'Mali'],
            ['Malta', 'Malta'],
            ['Mauricio', 'Mauritius'],
            ['Mauritania', 'Mauritia'],
            ['Islas Marshall', 'Marshall Island'],
            ['Estados Federados de Micronesia', 'Micronesia/Federated States of Micronesia'],
            ['México', 'Mexico'],
            ['Marruecos', 'Morocco'],
            ['Moldavia', 'Moldova'],
            ['Mónaco', 'Monaco'],
            ['Mongolia', 'Mongolia'],
            ['Montenegro', 'Montenegro'],
            ['Mozambique', 'Mozambique'],
            ['Myanmar', 'Myanmar'],
            ['Namibia', 'Namibia'],
            ['Nauru', 'Nauru'],
            ['Nepal', 'Nepal'],
            ['Nueva Zelanda', 'New Zealand'],
            ['Nicaragua', 'Nicaragua'],
            ['Níger', 'Niger'],
            ['Nigeria', 'Nigeria'],
            ['Niue', 'Niue'],
            ['Corea del Norte', 'North Korea'],
            ['Noruega', 'Norway'],
            ['Omán', 'Oman'],
            ['Palestina', 'Palestine'],
            ['Pakistán', 'Pakistan'],
            ['Palau', 'Palau'],
            ['Panamá', 'Panama'],
            ['Papúa Nueva Guinea', 'Papua New Guinea'],
            ['Paraguay', 'Paraguay'],
            ['Perú', 'Peru'],
            ['Filipinas', 'Philippines'],
            ['Polonia', 'Poland'],
            ['Portugal', 'Portugal'],
            ['Katar', 'Qatar'],
            ['Rumania', 'Romania'],
            ['Rusia', 'Russia'],
            ['Ruanda', 'Rwanda'],
            ['Samoa', 'Samoa'],
            ['Santa Lucía', 'Saint Lucia'],
            ['San Cristóbal y Nieves', 'Saint Kitts and Nevis'],
            ['San Marino', 'San Marino'],
            ['San Tome y Principe', 'Sao Tomé and Principe'],
            ['San Vicente y las Granadinas', 'Saint Vincent and the Grenadines'],
            ['Escocia', 'Scotland'],
            ['Senegal', 'Senegal'],
            ['Serbia', 'Serbia'],
            ['Seychelles', 'Seychelles'],
            ['Sierra Leona', 'Sierra Leone'],
            ['Singapur', 'Singapore'],
            ['Eslovaquia', 'Slovakia'],
            ['Islas Salomón', 'Solomon Islands'],
            ['Somalia', 'Somalia'],
            ['Sudáfrica', 'South Africa'],
            ['Corea del Sur', 'South Korea'],
            ['Sudán del Sur', 'South Sudan'],
            ['España', 'Spain'],
            ['Sri Lanka', 'Sri Lanka'],
            ['Sudán', 'Sudan'],
            ['Surinam', 'Suriname'],
            ['Swazilandia', 'Swaziland'],
            ['Suecia', 'Sweden'],
            ['Suiza', 'Switzerland'],
            ['Siria', 'Syria'],
            ['Tayikistán', 'Tajikistan'],
            ['Tanzania', 'Tanzania'],
            ['Tailandia', 'Thailand'],
            ['Togo', 'Togo'],
            ['Tonga', 'Tonga'],
            ['Trinidad y Tobago', 'Trinidad and Tobago'],
            ['Túnez', 'Tunisia'],
            ['Turkmenistán', 'Turkmenistan'],
            ['Turquia', 'Turkey'],
            ['Tuvalu', 'Tuvalu'],
            ['Ucrania', 'Ukraine'],
            ['Uganda', 'Uganda'],
            ['Uruguay', 'Uruguay'],
            ['Emiratos Árabes Unidos', 'United Arab Emirates'],
            ['Reino Unido', 'United Kingdom'],
            ['Estados Unidos', 'United States of America'],
            ['Uzbekistán', 'Uzbekistan'],
            ['Vanuatu', 'Vanuatu'],
            ['Venezuela', 'Venezuela'],
            ['Vietnam', 'Vietnam'],
            ['País de Gales', 'Wales'],
            ['Yemen', 'Yemen'],
            ['Zambia', 'Zambia'],
            ['Zimbabue', 'Zimbabwe'],
        ];

        $keyLocalizacao = app()->getLocale() == 'es' ? 0 : 1;

        $paisesLocalizados = [];
        foreach($paises as $pais) {
            $paisesLocalizados[$pais[0]] = $pais[$keyLocalizacao];
        }

        asort($paisesLocalizados);

        return $paisesLocalizados;
    }
}
